using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    // Start is called before the first frame update

    //SCORE
    public static int monstersKilled;

    private int monstersKilledInstanciado;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            monstersKilled = 0;
            monstersKilledInstanciado = 0;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void addKill()
    {
        instance.monstersKilledInstanciado += 1;
    }

    public static int GetMonstersKilled()
    {
        return instance.monstersKilledInstanciado;
    }
}

