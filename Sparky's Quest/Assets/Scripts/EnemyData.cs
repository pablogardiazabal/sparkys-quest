using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyData", menuName = "ScriptableObjects/Data Enemy", order = 1)]
public class EnemyData : ScriptableObject
{
    [SerializeField] private string name;
    [SerializeField] private float bounty;
    [SerializeField] private float speed;
    [SerializeField] private int life;

    public string Name { get => name; set => name = value; }
    public float Bounty { get => bounty; set => bounty = value; }
    public float Speed { get => speed; set => speed = value; }
    public int Life { get => life; set => life = value; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
