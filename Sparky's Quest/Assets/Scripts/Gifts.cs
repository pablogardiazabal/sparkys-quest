using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gifts : MonoBehaviour
{
    private WorldManager wm;
    [SerializeField] string type;
    // Start is called before the first frame update
    void Start()
    {
        wm = FindObjectOfType<WorldManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Debug.Log("GOT ME!");
            gameObject.SetActive(false);
            wm.AddInventoryGifts(type, gameObject);
        }
    }
}
