using DM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MageController : Enemy
{
    [SerializeField] private GameObject mage;
    [SerializeField] private GameObject raycastOrigin;
    [SerializeField] private float distanceRay = 5f;
    [SerializeField] private int magicCooldown = 4;

    [SerializeField] private float timerShoot = 0;
    [SerializeField] private GameObject magicPrefab;

    [SerializeField] bool canShoot = true;

    // Start is called before the first frame update
    void Start()
    {
        myAnim = mage.GetComponent<Animator>();

        if (canShoot && playerManager.Alive)
        {
            InvokeRepeating("RaycastMagic", 1f, 3f);
            RaycastMagic();
        }
        else
        {
            timerShoot += Time.deltaTime;
        }


        if (timerShoot > magicCooldown)
        {
            canShoot = true;
        }

    }

    private void RaycastMagic()
    {
        canShoot = false;
        
        RaycastHit hit;

        if (Physics.Raycast(raycastOrigin.transform.position, raycastOrigin.transform.TransformDirection(Vector3.forward), out hit, distanceRay))
        {
            Debug.Log(hit.transform.tag);
            if (hit.transform.tag == "Player")
            {
                myAnim.Play("Attack02");
                Debug.Log("Mi magia te destruirá");
                StartCoroutine(ShootMissile());
               
            }
        }

    }

    IEnumerator ShootMissile()
    {
        yield return new WaitForSeconds(2.15f);
        GameObject b = Instantiate(magicPrefab, raycastOrigin.transform.position, magicPrefab.transform.rotation);
        b.GetComponent<Rigidbody>().AddForce(raycastOrigin.transform.TransformDirection(Vector3.forward) * 10f, ForceMode.Impulse);
        timerShoot = 0;
    }

        private void OnDrawGizmos()
    {

        if (canShoot)
        {
            Gizmos.color = Color.blue;
            Vector3 direction = raycastOrigin.transform.TransformDirection(Vector3.forward) * distanceRay;
            Gizmos.DrawRay(raycastOrigin.transform.position, direction);
        }

    }

    

    // Update is called once per frame
    void Update()
    {
        base.Update();
    }
}
