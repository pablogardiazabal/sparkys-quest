using DM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    [SerializeField] protected string name;
    [SerializeField] protected float bounty;
    [SerializeField] protected int life;
    [SerializeField] protected float speed;
    [SerializeField] protected float range = 5f;
    [SerializeField] protected float attackRange = 3f;
    [SerializeField] protected GameObject player;
    [SerializeField] protected GameObject playerAnimator;
    [SerializeField] protected EnemyData myData;

    protected ControlManager playerManager;
    protected Animator myAnim;
    protected bool active = true;
    protected bool hitted = false;
    protected bool dead = false;
    protected bool attacking = false;
    protected float cooldown = 2f;
    protected float speedActual;
    protected float cooldownHit = 3f;

    // Start is called before the first frame update
    void Awake()
    { 
        playerManager = player.GetComponent<ControlManager>();
        playerManager.onDeath += Victory;
    }

    // Update is called once per frame
    protected void Update()
    {
        if (dead)
        {
            myAnim.Play("Die");
            StartCoroutine(destroyMe());
        }
    }

    IEnumerator destroyMe()
    {
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }

    private void Victory()
    {
        myAnim.Play("Victory");
    }

    public string GetLife()
    {
        return life.ToString();
    }

    public string GetBounty()
    {
        return bounty.ToString();
    }

    protected void ChasePlayer()
    {
        if (active && (transform.position - player.transform.position).magnitude <= range)
        {
            transform.LookAt(player.transform);
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, myData.Speed * Time.deltaTime);
            Debug.Log("CHASING");
            /*BLOQUE COMENTADO PARA TESTEAR
             * if ((transform.position - player.transform.position).magnitude <= attackRange)
            {
                myData.Speed = 0;
                attacking = true;
                myAnim.Play("Attack01");
                cooldown -= Time.deltaTime;
                if (cooldown <= 0)
                {
                    attacking = false;
                    cooldown = 2f;
                }

            }
            else
            {
                myData.Speed = speedActual;
            }*/
        }
    }

    protected void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Sword")
        {
            if (!hitted)
            {
                life--;
                hitted = true;
                myAnim.Play("GetHit");
                Debug.Log("ME PEGO");
                if (life <= 0)
                {
                    dead = true;
                }
            }

        }
    }
}
