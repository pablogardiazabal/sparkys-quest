using DM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldManager : MonoBehaviour
{
    [SerializeField] Dictionary<string, GameObject> Gifts = new Dictionary<string, GameObject>();
    [SerializeField] GameObject[] enemiesCount = new GameObject[5];
    [SerializeField] List<Bats> bats = new List<Bats>();
    [SerializeField] List<MageController> mages = new List<MageController>();
    [SerializeField] GameObject player;
    public TMPro.TextMeshProUGUI batsBounty;
    public TMPro.TextMeshProUGUI magesBounty;
    public TMPro.TextMeshProUGUI playerHealth;
    private ControlManager playerManager;
    // Start is called before the first frame update
    void Start()
    {
        HealthInfo();
        EnemiesInfo();
        playerManager = player.GetComponent<ControlManager>();
        playerManager.onDeath += Dead;
    }

    private void Dead()
    {
        Debug.Log("Player died");
        playerHealth.text = "YOU DIED";
    }

    // Update is called once per frame
    void Update()
    {
        if (playerManager.Alive)
        {
            playerHealth.text = "Life: " + player.GetComponent<ControlManager>().Life;
        }
        
    }

    void HealthInfo()
    {
        
        foreach (var bat in bats)
        {
            Debug.Log("Bats got " + bat.GetLife() + " health");
        }

        foreach (var mage in mages)
        {
            Debug.Log("Mages got " + mage.GetLife() + " health");
        }
    }

    void EnemiesInfo()
    {
        for (int i = 0; i < enemiesCount.Length; i++)
        {
            Bats CC = enemiesCount[i].GetComponent<Bats>();
            if (CC)
            {
                batsBounty.text = CC.name + " has a bounty of: " + CC.GetBounty();
                Debug.Log(CC.name + " has a bounty of: " + CC.GetBounty());
            }
            if (!CC)
            {
                magesBounty.text = enemiesCount[i].GetComponent<MageController>().name + " has a bounty of: " + enemiesCount[i].GetComponent<MageController>().GetBounty();
                Debug.Log(enemiesCount[i].GetComponent<MageController>().name + " has a bounty of: " + enemiesCount[i].GetComponent<MageController>().GetBounty());
            }
        }
    }

    public void AddInventoryGifts(string key, GameObject item)
    {
        Gifts.Add(key, item);
    }

    public GameObject GetInventoryGifts(string key)
    {
        return Gifts[key] as GameObject;
    }

    public void SeeInventoryGifts()
    {
        Debug.Log(Gifts.ToString());
        foreach (var item in Gifts)
        {
            Debug.Log(item.ToString());
        }
    }

    public bool GetGifts()
    {
        return Gifts.Count > 0;
    }
}
