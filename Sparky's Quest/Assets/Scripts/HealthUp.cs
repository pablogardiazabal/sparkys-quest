using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealthUp : MonoBehaviour
{
    [SerializeField] private UnityEvent onHealthBoost;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("BonusHealth");
            onHealthBoost?.Invoke();
            Destroy(gameObject);
        }
    }
}
