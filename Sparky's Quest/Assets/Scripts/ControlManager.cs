﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using TMPro;

namespace DM
{
    public class ControlManager : MonoBehaviour
    {
        [Header("Initialize")]
        [SerializeField] GameObject activeModel; 
        [SerializeField] string[] randomAttacks; 

        [Header("Inputs")]
        [SerializeField] float vertical;  
        [SerializeField] float horizontal; 
        [SerializeField] float moveAmount;   
        [SerializeField] Vector3 moveDir;    

        [Header("Stats")]
        [SerializeField] float moveSpeed = 3.5f;  
        [SerializeField] float sprintSpeed = 5f;  
        [SerializeField] float rotateSpeed = 5;   
        [SerializeField] float jumpForce = 600f;  
        

        [Header("States")]
        [SerializeField] bool onGround;  
        [SerializeField] bool sprint;     
        [HideInInspector]
        [SerializeField] bool jump;      
        [HideInInspector]
        [SerializeField] bool normalAttack;   
        [HideInInspector]
        [SerializeField] bool comboAttack;      
        [SerializeField] bool canMove; 
        [HideInInspector]
        [SerializeField] bool roll;      
        [SerializeField] bool dead; 
        [SerializeField] AudioClip swordAttack;
        AudioSource myAudioSource;
        [SerializeField] Collider weaponCollider;
        [SerializeField] int life = 10;
        bool alive = true;

        public event Action onDeath;

        
        float delta;       
        Animator anim;     
        [HideInInspector]
        public Rigidbody rigid;     
        CameraManager camManager;   

        public bool CanMove { get => canMove; set => canMove = value; }
        public bool OnGround { get => onGround; set => onGround = value; }
        public int Life { get => life; set => life = value; }
        public bool Alive { get => alive; set => alive = value; }

        void Start() 
        {
            weaponCollider.enabled = false;
            myAudioSource = gameObject.GetComponent<AudioSource>();
            camManager = CameraManager.singleton;
            camManager.Init(this.transform);
            SetupAnimator();
            rigid = GetComponent<Rigidbody>();            
        }
        
        void SetupAnimator()
        {
            if (activeModel == null)
            {
                anim = GetComponentInChildren<Animator>();
                if (anim == null)
                {
                    Debug.Log("No model");
                }
                else
                {
                    activeModel = anim.gameObject; 
                }
            }

            if (anim == null)
                anim = activeModel.GetComponent<Animator>();            
        }
        
        void FixedUpdate () 
        {
            delta = Time.fixedDeltaTime;       

            FixedTick(delta);  
            camManager.FixedTick(delta);     
        }
        
        void Update()
        {
            switch (alive)
            {
                case true:
                    GetInput();
                    UpdateStates();
                    break;
              
                case false:
                    Debug.Log("Te moriste");
                    break;
                default:
                    Debug.Log("default");
                    break;
            } 

            if(life <= 0)
            {
                anim.SetBool("dead", true);
                alive = false;
                onDeath();
                StartCoroutine(destroyMe());
            }
        }

        public void HealthBoost()
        {
            life += 10;
        }

        IEnumerator destroyMe()
        {
            yield return new WaitForSeconds(4);
            Destroy(activeModel);
        }

        void GetInput() 
        {
           vertical = Input.GetAxis("Vertical");    
           horizontal = Input.GetAxis("Horizontal");   
           sprint = Input.GetButton("SprintInput");     
           jump = Input.GetButtonDown("Jump");     
           normalAttack = Input.GetButtonDown("Fire1"); 
           comboAttack = Input.GetButtonDown("Fire2");    
           roll = Input.GetButtonDown("Fire3");     
        }
        
        void UpdateStates()
        {
            canMove = anim.GetBool("canMove");             
                
            if (jump)   
            {
                if (onGround && canMove) 
                {
                    anim.CrossFade("falling", 0.1f);
                    rigid.AddForce(0, jumpForce, 0);                  
                }            
            }   

            if(comboAttack)    
            {
                if(onGround)    
                {
                    weaponCollider.enabled = true;
                    myAudioSource.PlayOneShot(swordAttack);
                    anim.SetTrigger("combo");
                    StartCoroutine(DisableWeaponCollider());
                }
            }

            if(roll && onGround)   
            {                
                anim.SetTrigger("roll");   
            }            
            
            float targetSpeed = moveSpeed;  
             
            if (sprint)
            {
                targetSpeed = sprintSpeed;               
            }

      
            Vector3 v = vertical * camManager.transform.forward;
            Vector3 h = horizontal * camManager.transform.right;            

           
            moveDir = ((v + h).normalized) * (targetSpeed * moveAmount);            

     
            moveDir.y = rigid.velocity.y;            
            
           
            float m = Mathf.Abs(horizontal) + Mathf.Abs(vertical);
            moveAmount = Mathf.Clamp01(m);
            
            if (normalAttack && canMove) 
            {
                weaponCollider.enabled = true;
                string targetAnim;

                int r = UnityEngine.Random.Range(0, randomAttacks.Length);
                targetAnim = randomAttacks[r];

                anim.CrossFade(targetAnim, 0.1f);           

                myAudioSource.PlayOneShot(swordAttack);

                if (!onGround)
                {
                    anim.CrossFade("JumpAttack", 0.1f);
                }
                StartCoroutine(DisableWeaponCollider());
                normalAttack = false;
            } 
                       
        }

        private IEnumerator DisableWeaponCollider()
        {
            yield return new WaitForSeconds(1f);
            weaponCollider.enabled = false;
        }

        void FixedTick(float d)
        {
            delta = d;            

            if (onGround)
            {
                if(canMove)
                {
                    rigid.velocity = moveDir;          
                }                 
            }            

           
            if (canMove)
            {
                Vector3 targetDir = moveDir;
                targetDir.y = 0;
                if (targetDir == Vector3.zero)
                    targetDir = transform.forward;

                Quaternion tr = Quaternion.LookRotation(targetDir);
                Quaternion targetRotation = Quaternion.Slerp(transform.rotation, tr, delta * moveAmount * rotateSpeed);
                transform.rotation = targetRotation;
            }  

            HandleMovementAnimations();
        }

        void HandleMovementAnimations()
        {
            
            anim.SetBool("sprint", sprint);  
            if(moveAmount == 0)
            {
                anim.SetBool("sprint", false);
            }            
            
            anim.SetFloat("vertical", moveAmount, 0.2f, delta);
        }


        private void OnCollisionEnter(Collision other)
        {
            if(other.gameObject.tag == "Enemy")
            {
                Debug.Log("ENEMY");
            }
        }

        private void OnTriggerStay(Collider collision)
        {
            if (collision.gameObject.tag == "Ground")
            {
                onGround = true;                
                anim.SetBool("onGround", true);                
            }
        }

    
        private void OnTriggerExit(Collider collision)
        {
            if (collision.gameObject.tag == "Ground")
            {
                onGround = false;
                anim.SetBool("onGround", false);                
            }
        }        

    }
}


