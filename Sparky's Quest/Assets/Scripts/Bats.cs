using DM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bats : Enemy
{
    private Patrol patrol;

    // Start is called before the first frame update
    void Start()
    {
        patrol = gameObject.GetComponent<Patrol>();
        myAnim = gameObject.GetComponent<Animator>();
        speedActual = speed;
    }

    public void inactiveMe()
    {
        active = false;
        myAnim.Play("IdleNormal");
    }

    // Update is called once per frame
    void Update()
    {
        base.Update();
        if (!dead)
        {
            ChasePlayer();
        }

        if (hitted)
        {
            cooldownHit -= Time.deltaTime;
        }

        if (cooldownHit <= 0)
        {
            hitted = false;
            cooldownHit = 3f;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        base.OnCollisionEnter(other);
        if(other.gameObject.tag == "Player" && attacking)
        {
            Debug.Log("LE PEGUE");
            playerAnimator.GetComponent<Animator>().Play("Dizzy");
            player.GetComponent<ControlManager>().Life -= 1;
        }
    }


 
}
